/**
 *@author Natalie Stephenson
 *"AI" that is not really an AI, but a program that chooses moves as an 
 * opponent in Gomoku. Gomobot!
 *
 * UPDATE 5-19-18 (EMW): Made Beta Version of Hard AI, has multiple 
 *                      shortcomings, but is marginally smarter that 
 *                      the medium bot.
 * 
 */

import java.util.Random; 
public class Gomobot{

	final int BOARDSIZE = 30;
	final int SEARCH_RADIUS = 3;
	private int[][] board;
	private PlayGameController controller; 
	private String difficulty; 
	private int pieceColor;
	private int numMoves;
	private int pieces;
	private int[] enemyLastMove;
	private int[] myLastMove;
	private int[] hardMove;


	//constructor for AI
	public Gomobot(PlayGameController p, String diff){
		difficulty = diff;
		controller = p; 
		board = new int[BOARDSIZE][BOARDSIZE];
		pieceColor = controller.getPieceColor();
		
		enemyLastMove = new int[2];
		myLastMove = new int[2];
		hardMove = new int[2];
	}

	
	/**
	 * Called by the PlayGameController on this AI's 
	 * turn, chooses a type of move to make based on
	 * the set difficult of this AI.
	 */
	/**
	 * 
	 */
	public void startTurn(){
		if(this.difficulty.equals("EASY")){
			makeMoveEasy(); 
		}
		else if(this.difficulty.equals("MEDIUM")){
			makeMoveMedium(); 
		}
		else if(this.difficulty.equals("HARD")){
			makeMoveHard(); 
		}

	}


	/**
	 * The move generator for the EASY Gomobot. Randomly selects 
	 * a place on the board to move and checks if it is clear. 
	 * If so, it send the move to the controller.
	 */
	private void makeMoveEasy(){

		//something like this
		boolean done = false; 
		int r = -2,c = -2;
		while(!done){
			r = (int) Math.floor(Math.random()*BOARDSIZE);
			c = (int) Math.floor(Math.random()*BOARDSIZE);
			done = isOpen(r, c);
		}
		numMoves++;
		controller.makeMove(r,c); 
		board[r][c] = pieceColor;
		myLastMove[0] = r;
		myLastMove[1] = c;
	}

	/**
	 * Helper function to determine if a board location is Free 
	 * to play a piece
	 * @param r a given row on the board
	 * @param c a given column on the board
	 * @return a boolean referring to whether or not the location is open
	 */
	private boolean isOpen(int r, int c){
		boolean ret = false;
		if(board[r][c] == 0)
			ret = true;
		return ret;
	}

	/**
	 * Called by makeMove at the beginning of the AI's turn when 
	 * the difficult is set to Medium.
	 */
	private void makeMoveMedium(){

		// choose a random location
		int startC = (int) Math.floor(Math.random()*BOARDSIZE); 
		int startR = (int) Math.floor(Math.random()*BOARDSIZE); 

		if(numMoves == 0) {
			makeMoveEasy();
		}else {
			int[] move = findPiece(startR, startC);

			if(move != null) {
				numMoves++;
				controller.makeMove(move[0], move[1]);
				board[move[0]][move[1]] = pieceColor;
				myLastMove[0] = move[0];
				myLastMove[1] = move[1];
			}else {
				makeMoveEasy();
			}

		}	
	}


	/**
	 * Finds a piece played previously by this AI to play 
	 * next to. Called when making a Medium move.
	 * 
	 * @param startR
	 * @param startC
	 * @return
	 */
	private int[] findPiece(int startR, int startC) {

		int[] arr = new int[]{0,0};
		int r = (startR+1)%BOARDSIZE, c = 0;
		boolean doneSearching = false, done = false;
		int row,col;
		while(!doneSearching) {

			// Find a piece of my color
			while(!done) {
				while(!done && c < BOARDSIZE && r < BOARDSIZE) {
					if(board[r][c] == pieceColor){
						arr[0] = r;
						arr[1] = c;
						done = true;
					}else {
						c++;
					}
				}
				c = 0;
				r = (r+1)%BOARDSIZE;
			}

			// Find an empty spot around it
			row = arr[0];
			col = arr[1];
			if(row>0 && col>0 && board[row-1][col-1]==0) {
				arr[0] = row-1;
				arr[1] = col-1;
				doneSearching = true;
			}else if(col>0 && board[row][col-1]==0)  {
				arr[0] = row;
				arr[1] = col-1;
				doneSearching = true;
			}else if(row<BOARDSIZE && col>0 && board[row+1][col-1]==0) {
				arr[0] = row+1;
				arr[1] = col-1;
				doneSearching = true;
			}else if(row>0 && board[row-1][col]==0) {
				arr[0] = row-1;
				arr[1] = col;
				doneSearching = true;
			}else if(row<BOARDSIZE && board[row+1][col]==0) {
				arr[0] = row+1;
				arr[1] = col;
				doneSearching = true;
			}else if(row>0 && col<BOARDSIZE && board[row-1][col+1]==0) {
				arr[0] = row-1;
				arr[1] = col+1;
				doneSearching = true;
			}else if(col<BOARDSIZE && board[row][col+1]==0) {
				arr[0] = row;
				arr[1] = col+1;
				doneSearching = true;
			}else if(col<BOARDSIZE && row<BOARDSIZE && board[row+1][col+1]==0) {
				arr[0] = row+1;
				arr[1] = col+1;
				doneSearching = true;
			}
			if(!doneSearching) {
				doneSearching = true;
				done = false;
			}
		}
		if(!done)
			return null;
		else
			return arr;
	}

	/**
	 * Called by makeMove at the beginning of the AI's turn when 
	 * the difficult is set to Hard. Has a set priority list and 
	 * attempts to make moves based on the last piece played by 
	 * each player.
	 */
	private void makeMoveHard(){
		
		int enemyColor = 1;

		if(this.pieceColor ==  1)
			enemyColor = 2;

		// 0.) If first move, make easy move.
		if(numMoves == 0) {
			makeMoveEasy();
		}else {
			if(lookForWin(this.myLastMove, this.pieceColor)) {
				// 1.) Look for my 4 in a row
				numMoves++;
				controller.makeMove(hardMove[0], hardMove[1]); 
				board[hardMove[0]][hardMove[1]] = this.pieceColor;
				myLastMove[0] = hardMove[0];
				myLastMove[1] = hardMove[1];
			}else if(lookForWin(this.enemyLastMove, enemyColor)) {
				// 2.) Look for opponent 4 in a row
				numMoves++;
				controller.makeMove(hardMove[0], hardMove[1]); 
				board[hardMove[0]][hardMove[1]] = this.pieceColor;
				myLastMove[0] = hardMove[0];
				myLastMove[1] = hardMove[1];

			}else if(lookForThree(this.enemyLastMove, enemyColor)) {
				
				// 3.) Look for opponent 3 in a row
				numMoves++;
				controller.makeMove(hardMove[0], hardMove[1]); 
				board[hardMove[0]][hardMove[1]] = this.pieceColor;
				myLastMove[0] = hardMove[0];
				myLastMove[1] = hardMove[1];
			}else if(lookForThree(this.myLastMove, this.pieceColor)) {
				// 4.) Look for my 3 in a row
				numMoves++;
				controller.makeMove(hardMove[0], hardMove[1]); 
				board[hardMove[0]][hardMove[1]] = this.pieceColor;
				myLastMove[0] = hardMove[0];
				myLastMove[1] = hardMove[1];
			}else if(lookForTwo(this.myLastMove, this.pieceColor)) {
				// 5.) Look for my 2 in a row
				numMoves++;
				controller.makeMove(hardMove[0], hardMove[1]); 
				board[hardMove[0]][hardMove[1]] = this.pieceColor;
				myLastMove[0] = hardMove[0];
				myLastMove[1] = hardMove[1];
			}else {
				// 6.) No better ideas, make Medium move
				makeMoveMedium();
			}
		}

		
		
	}

	/**
	 * Checks if the most recent piece played for the color 
	 * sent in has put them in a state that is one move away 
	 * from winning.  If found, a piece is placed at the end 
	 * of the line to either win the game or keep the oppenant 
	 * from winning.
	 * 
	 * @param move the latest move by a player
	 * @param color that players color
	 * @return a boolean referring to if the condition was found.
	 */
	private boolean lookForWin(int[] move, int color) {
		int endCon = 4;
		boolean found = false;
		int x = move[0], y = move[1];
		pieces = 0;
		boolean r = false;
		boolean done = false;
		if( board[x][y] == color ) {
			pieces++;
			// Search the board horizontally right:
			if( y+1 < BOARDSIZE && board[x][y+1] == color) {
				pieces++;
				if( pieces == 4 ) {
					//find open spot
					if( y+2 < BOARDSIZE && board[x][y+2] == 0 ) {
						hardMove[0] = x;
						hardMove[1] = y+2;
						r = true;
					}
					int i = 0;
					found = false;
					while(i < endCon && !found) {
						if((y-i) >= 0 && board[x][y-i] == 0) {
							hardMove[0] = x;
							hardMove[1] = y-i;
							r = true;
							found = true;
						}
						
						i++;
					}
				}
				if( y+2 < BOARDSIZE && board[x][y+2] == color ) {
					pieces++;
					if( pieces == 4 ) {
						//check for open spot
						if( y+3 < BOARDSIZE && board[x][y+3] == 0 ) {
							hardMove[0] = x;
							hardMove[1] = y+3;
							r = true;
						}	
						int i = 0;
						found = false;
						while(i < endCon && !found) {
							if((y-i) >= 0 && board[x][y-i] == 0) {
								hardMove[0] = x;
								hardMove[1] = y-i;
								r = true;
								found = true;
							}
							
							i++;
						}
					}
					if( y+3 < BOARDSIZE && board[x][y+3] == color ) {
						pieces++;
						if( pieces == 4 ) {
							//check for open spot
							if( y+4 < BOARDSIZE && board[x][y+4] == 0 ) {
								hardMove[0] = x;
								hardMove[1] = y+4;
								r = true;
							}	
							int i = 0;
							found = false;
							while(i < endCon && !found) {
								if((y-i) >= 0 && board[x][y-i] == 0) {
									hardMove[0] = x;
									hardMove[1] = y-i;
									r = true;
									found = true;
								}
								
								i++;
							}
						} } } }
			
			// Search the board horizontally left:
			if( y-1 >= 0 && board[x][y-1] == color ) {
				pieces++;
				if( pieces == 4 ) {
					//check for open
					if( y-2 >= BOARDSIZE && board[x][y-2] == 0 ) {
						hardMove[0] = x;
						hardMove[1] = y-2;
						r = true;
					}	
					int i = 0;
					found = false;
					while(i < endCon && !found) {
						if((y+i) <= BOARDSIZE && board[x][y+i] == 0) {
							hardMove[0] = x;
							hardMove[1] = y+i;
							r = true;
							found = true;
						}
						
						i++;
					}
				}
				if( y-2 >= 0 && board[x][y-2] == color ) {
					pieces++;
					if( pieces == 4 ) {
						//check if next is open
						if( y-3 >= 0 && board[x][y-3] == 0 ) {
							hardMove[0] = x;
							hardMove[1] = y-3;
							r = true;
						}	
						int i = 0;
						found = false;
						while(i < endCon && !found) {
							if((y+i) <= BOARDSIZE && board[x][y+i] == 0) {
								hardMove[0] = x;
								hardMove[1] = y+i;
								r = true;
								found = true;
							}
							
							i++;
						}
					}
					if( y-3 >= 0 && board[x][y-3] == color ) {
						pieces++;
						if( pieces == 4 ) {
							//check if next is open
							if( y-4 >= 0 && board[x][y-4] == 0 ) {
								hardMove[0] = x;
								hardMove[1] = y-4;
								r = true;
							}	
							int i = 0;
							found = false;
							while(i < endCon && !found) {
								if((y+i) <= BOARDSIZE && board[x][y+i] == 0) {
									hardMove[0] = x;
									hardMove[1] = y+i;
									r = true;
									found = true;
								}
								
								i++;
							}
						} } } }	
		}else {
			pieces = 0;
		}
		// Searched Horizontally 
		pieces = 0;
		if( board[x][y] == color ) {
			pieces++;
			// Search the board vertically up:
			if( x-1 >= 0 && board[x-1][y] == color ) {
				pieces++;
				if( pieces == 4 ) {
					//check if next is open
					if( x-2 >= 0 && board[x-2][y] == 0 ) {
						hardMove[0] = x-2;
						hardMove[1] = y;
						r = true;
					}
					
					
				}
				if( x-2 >= 0 && board[x-2][y] == color ) {
					pieces++;
					if( pieces == 4 ) {
						//check if next is open
						if( x-3 >= 0 && board[x-3][y] == 0 ) {
							hardMove[0] = x-3;
							hardMove[1] = y;
							r = true;
						}	
					}
					if( x-3 >= 0 && board[x-3][y] == color ) {
						pieces++;
						if( pieces == 4 ) {
							//check if next is open
							if( x-4 >= 0 && board[x-4][y] == 0 ) {
								hardMove[0] = x-4;
								hardMove[1] = y;
								r = true;
							}	
						} } } }
			// Search the board vertically down:
			if( x+1 < BOARDSIZE && board[x+1][y] == color ) {
				pieces++;
				if( pieces == 4 ) {
					//check if next is open
					if( x+2 < BOARDSIZE && board[x+2][y] == 0 ) {
						hardMove[0] = x+2;
						hardMove[1] = y;
						r = true;
					}	
					int i = 0;
					found = false;
					while(i < endCon && !found) {
						if((x-i) >= 0 && board[x-i][y] == 0) {
							hardMove[0] = x-i;
							hardMove[1] = y;
							r = true;
							found = true;
						}
						
						i++;
					}
				}
				if( x+2 < BOARDSIZE && board[x+2][y] == color ) {
					pieces++;
					if( pieces == 4 ) {
						//check if next is open
						if( x+3 < BOARDSIZE && board[x+3][y] == 0 ) {
							hardMove[0] = x+3;
							hardMove[1] = y;
							r = true;
						}	
						int i = 0;
						found = false;
						while(i < endCon && !found) {
							if((x-i) >= 0 && board[x-i][y] == 0) {
								hardMove[0] = x-i;
								hardMove[1] = y;
								r = true;
								found = true;
							}
							
							i++;
						}
					}
					if( x+3 < BOARDSIZE && board[x+3][y] == color ) {
						pieces++;
						if( pieces == 4 ) {
							//check if next is open
							if( x+4 < BOARDSIZE && board[x+4][y] == 0 ) {
								hardMove[0] = x+4;
								hardMove[1] = y;
								r = true;
							}
							int i = 0;
							found = false;
							while(i < endCon && !found) {
								if((x-i) >= 0 && board[x-i][y] == 0) {
									hardMove[0] = x-i;
									hardMove[1] = y;
									r = true;
									found = true;
								}
								
								i++;
							}
						} } } } 
		}else {
			pieces = 0;
		}
		//Searched Vertically 
		pieces = 0;
		
		
		if( board[x][y] == color ) {
			//increment number of pieces in a row 
			pieces++;
			// searches the board up to the left == check for 5 pieces in a row after each new piece found
			if( x-1 >= 0 && y-1 >= 0 && board[x-1][y-1] == color   ) {
				pieces++;
				if( pieces == 4 ) {
					//check if next is open
					if( x-2 >= 0 && y-2 >= 0 && board[x-2][y-2] == 0 ) {
						hardMove[0] = x-2;
						hardMove[1] = y-2;
						r = true;
					}	
				}
				if( x-2 >= 0 && y-2 >= 0 && board[x-2][y-2] == color ) {
					pieces++;
					if( pieces == 4 ) {
						//check if next is open
						if( x-3 >= 0 && y-3 >= 0 && board[x-3][y-3] == 0 ) {
							hardMove[0] = x-3;
							hardMove[1] = y-3; 
							r = true;
						}	
					}
					if( x-3 >= 0 && y-3 >= 0 && board[x-3][y-3] == color ) {
						pieces++;
						if( pieces == 4 ) {
							//check if next is open
							if( x-4 >= 0 && y-4 >= 0 && board[x-4][y-4] == 0) {
								hardMove[0] = x-4;
								hardMove[1] = y-4; 
								r = true;
							}	
						} } } } 
			
			// searches the board down to the right
			if( x+1 < BOARDSIZE && y+1 < BOARDSIZE  && board[x+1][y+1] == color ) {
				pieces++;
				if( pieces == 4 ) {
					//check if next is open
					if( x+2 < BOARDSIZE && y+2 < BOARDSIZE && board[x+2][y+2] == 0 ) {
						hardMove[0] = x+2;
						hardMove[1] = y+2; 
						r = true;
					}	
					int i = 0;
					found = false;
					while(i < endCon && !found) {
						if((x-i) >= 0 && (y-i) >= 0 && board[x-i][y-i] == 0) {
							hardMove[0] = x-i;
							hardMove[1] = y-i;
							r = true;
							found = true;
						}
						
						i++;
					}
				}
				if( x+2 < BOARDSIZE && y+2 < BOARDSIZE && board[x+2][y+2] == color ) {
					pieces++;
					if( pieces == 4 ) {
						//check if next is open
						if( x+3 < BOARDSIZE && y+3 < BOARDSIZE && board[x+3][y+3] == 0 ) {
							hardMove[0] = x+3;
							hardMove[1] = y+3; 
							r = true;
						}
						int i = 0;
						found = false;
						while(i < endCon && !found) {
							if((x-i) >= 0 && (y-i) >= 0 && board[x-i][y-i] == 0) {
								hardMove[0] = x-i;
								hardMove[1] = y-i;
								r = true;
								found = true;
							}
							
							i++;
						}
					}
					if( x+3 < BOARDSIZE && y+3 < BOARDSIZE && board[x+3][y+3] == color ) {
						pieces++;
						if( pieces == 4 ) {
							//check if next is open
							if( x+4 < BOARDSIZE && y+4 < BOARDSIZE && board[x+4][y+4] == 0 ) {
								hardMove[0] = x+4;
								hardMove[1] = y+4; 
								r = true;
							}	
							int i = 0;
							found = false;
							while(i < endCon && !found) {
								if((x-i) >= 0 && (y-i) >= 0 && board[x-i][y-i] == 0) {
									hardMove[0] = x-i;
									hardMove[1] = y-i;
									r = true;
									found = true;
								}
								
								i++;
							}
						} } } } 
		} else {
			pieces = 0;
		}
		pieces = 0;
		
		if( board[x][y] == color ) {
			//increment number of pieces in a row 
			pieces++;
			// searches the board up to the right == check for 5 pieces in a row after each new piece found
			if( x-1 >= 0 && y+1 < BOARDSIZE && board[x-1][y+1] == color ) {
				pieces++;
				if( pieces == 4 ) {
					//check if next is open
					if( x-2 >= 0 && y+2 < BOARDSIZE && board[x-2][y+2] == 0 ) {
						hardMove[0] = x-2;
						hardMove[1] = y+2; 
						r = true;
					}	
				}
				if( x-2 >= 0 && y+2 < BOARDSIZE && board[x-2][y+2] == color ) {
					pieces++;
					if( pieces == 4 ) {
						//check if next is open
						if( x-3 >= 0 && y+3 < BOARDSIZE && board[x-3][y+3] == 0 ) {
							hardMove[0] = x-3;
							hardMove[1] = y+3; 
							r = true;
						}	
					}
					if(x-3 >= 0 && y+3 < BOARDSIZE && board[x-3][y+3] == color ) {
						pieces++;
						if( pieces == 4 ) {
							//check if next is open
							if( x-4 >= 0 && y+4 < BOARDSIZE && board[x-4][y+4] == 0 ) {
								hardMove[0] = x-4;
								hardMove[1] = y+4; 
								r = true;
							}	
						} } } } // searches the board down to the left
			if( x+1 < BOARDSIZE && y-1 >= 0 && board[x+1][y-1] == color ) {
				pieces++;
				if( pieces == 4 ) {
					//check if next is open
					if( x+2 >= 0 && y-2 < BOARDSIZE && board[x+2][y-2] == 0 ) {
						hardMove[0] = x+2;
						hardMove[1] = y-2; 
						r = true;
					}	
					int i = 0;
					found = false;
					while(i < endCon && !found) {
						if((x-i) >= 0 && (y+i) < BOARDSIZE && board[x-i][y+i] == 0) {
							hardMove[0] = x-i;
							hardMove[1] = y+i;
							r = true;
							found = true;
						}
						
						i++;
					}
				}
				if( x+2 < BOARDSIZE && y-2 >= 0 && board[x+2][y-2] == color ) {
					pieces++;
					if( pieces == 4 ) {
						//check if next is open
						if( x+3 >= 0 && y-3 < BOARDSIZE && board[x+3][y-3] == 0 ) {
							hardMove[0] = x+3;
							hardMove[1] = y-3; 
							r = true;
						}
						int i = 0;
						found = false;
						while(i < endCon && !found) {
							if((x-i) >= 0 && (y+i) < BOARDSIZE && board[x-i][y+i] == 0) {
								hardMove[0] = x-i;
								hardMove[1] = y+i;
								r = true;
								found = true;
							}
							
							i++;
						}
					}
					if( x+3 < BOARDSIZE && y-3 >= 0 && board[x+3][y-3] == color ) {
						pieces++;
						if( pieces == 4 ) {
							//check if next is open
							if( x+4 >= 0 && y-4 < BOARDSIZE && board[x+4][y-4] == 0 ) {
								hardMove[0] = x+4;
								hardMove[1] = y-4; 
								r = true;
							}	
							int i = 0;
							found = false;
							while(i < endCon && !found) {
								if((x-i) >= 0 && (y+i) < BOARDSIZE && board[x-i][y+i] == 0) {
									hardMove[0] = x-i;
									hardMove[1] = y+i;
									r = true;
									found = true;
								}
								
								i++;
							}
						} } } } 
		} else{
			pieces = 0;
		}
		
		
		pieces = 0;
		return r;

	}

	/**
	 * Checks if the most recent piece played for the color 
	 * sent in has put them in a state that is two moves away 
	 * from winning.  If found, a piece is placed at the end 
	 * of the line to either work towards winning the game or 
	 * keep the opponent from progressing.
	 * 
	 * @param move the latest move by a player
	 * @param color that players color
	 * @return a boolean referring to if the condition was found.
	 */
	private boolean lookForThree(int[] move, int color) {
		int endCon = 3;
		boolean found = false;
		int x = move[0], y = move[1];
		pieces = 0;
		boolean r = false;
		boolean done = false;
		if( board[x][y] == color ) {
			pieces++;
			// Search the board horizontally right:
			if( y+1 < BOARDSIZE && board[x][y+1] == color) {
				pieces++;
				if( pieces == endCon ) {
					//find open spot
					if( y+2 < BOARDSIZE && board[x][y+2] == 0 ) {
						hardMove[0] = x;
						hardMove[1] = y+2;
						r = true;
					}
					int i = 0;
					found = false;
					while(i < endCon && !found) {
						
						if((y-i) >= 0 && board[x][y-i] == 0) {
							hardMove[0] = x;
							hardMove[1] = y-i;
							r = true;
							found = true;
						}
						
						i++;
					}
				}
				if( y+2 < BOARDSIZE && board[x][y+2] == color ) {
					pieces++;
					if( pieces == endCon ) {
						//check for open spot
						if( y+3 < BOARDSIZE && board[x][y+3] == 0 ) {
							hardMove[0] = x;
							hardMove[1] = y+3;
							r = true;
						}	
						int i = 0;
						found = false;
						while(i < endCon && !found) {
							
							if((y-i) >= 0 && board[x][y-i] == 0) {
								hardMove[0] = x;
								hardMove[1] = y-i;
								r = true;
								found = true;
							}
							
							i++;
						}
					} } }
			
			// Search the board horizontally left:
			if( y-1 >= 0 && board[x][y-1] == color ) {
				pieces++;
				if( pieces == endCon ) {
					//check for open
					if( y-2 >= BOARDSIZE && board[x][y-2] == 0 ) {
						hardMove[0] = x;
						hardMove[1] = y-2;
						r = true;
					}	
					int i = 0;
					found = false;
					while(i < endCon && !found) {
						
						if((y+i) <= BOARDSIZE && board[x][y+i] == 0) {
							hardMove[0] = x;
							hardMove[1] = y+i;
							r = true;
							found = true;
						}
						
						i++;
					}
				}
				if( y-2 >= 0 && board[x][y-2] == color ) {
					pieces++;
					if( pieces == endCon ) {
						//check if next is open
						if( y-3 >= 0 && board[x][y-3] == 0 ) {
							hardMove[0] = x;
							hardMove[1] = y-3;
							r = true;
						}	
						int i = 0;
						found = false;
						while(i < endCon && !found) {
							
							if((y+i) <= BOARDSIZE && board[x][y+i] == 0) {
								hardMove[0] = x;
								hardMove[1] = y+i;
								r = true;
								found = true;
							}
							
							i++;
						}
					} } }	
		}else {
			pieces = 0;
		}
		// Searched Horizontally 
		
		pieces = 0;
		if( board[x][y] == color ) {
			pieces++;
			// Search the board vertically up:
			if( x-1 >= 0 && board[x-1][y] == color ) {
				pieces++;
				if( pieces == endCon ) {
					//check if next is open
					if( x-2 >= 0 && board[x-2][y] == 0 ) {
						hardMove[0] = x-2;
						hardMove[1] = y;
						r = true;
					}
					
					
				}
				if( x-2 >= 0 && board[x-2][y] == color ) {
					pieces++;
					if( pieces == endCon ) {
						//check if next is open
						if( x-3 >= 0 && board[x-3][y] == 0 ) {
							hardMove[0] = x-3;
							hardMove[1] = y;
							r = true;
						}	
					} } }
			// Search the board vertically down:
			if( x+1 < BOARDSIZE && board[x+1][y] == color ) {
				pieces++;
				if( pieces == endCon ) {
					//check if next is open
					if( x+2 < BOARDSIZE && board[x+2][y] == 0 ) {
						hardMove[0] = x+2;
						hardMove[1] = y;
						r = true;
					}	
					int i = 0;
					found = false;
					while(i < endCon && !found) {
						
						if((x-i) >= 0 && board[x-i][y] == 0) {
							hardMove[0] = x-i;
							hardMove[1] = y;
							r = true;
							found = true;
						}
						
						i++;
					}
				}
				if( x+2 < BOARDSIZE && board[x+2][y] == color ) {
					pieces++;
					if( pieces == endCon ) {
						//check if next is open
						if( x+3 < BOARDSIZE && board[x+3][y] == 0 ) {
							hardMove[0] = x+3;
							hardMove[1] = y;
							r = true;
						}	
						int i = 0;
						found = false;
						while(i < endCon && !found) {
							
							if((x-i) >= 0 && board[x-i][y] == 0) {
								hardMove[0] = x-i;
								hardMove[1] = y;
								r = true;
								found = true;
							}
							
							i++;
						}
					} } } 
		}else {
			pieces = 0;
		}
		//Searched Vertically 
		pieces = 0;
		
		
		if( board[x][y] == color ) {
			//increment number of pieces in a row 
			pieces++;
			// searches the board up to the left == check for 5 pieces in a row after each new piece found
			if( x-1 >= 0 && y-1 >= 0 && board[x-1][y-1] == color   ) {
				pieces++;
				if( pieces == endCon ) {
					//check if next is open
					if( x-2 >= 0 && y-2 >= 0 && board[x-2][y-2] == 0 ) {
						hardMove[0] = x-2;
						hardMove[1] = y-2;
						r = true;
					}	
				}
				if( x-2 >= 0 && y-2 >= 0 && board[x-2][y-2] == color ) {
					pieces++;
					if( pieces == endCon ) {
						//check if next is open
						if( x-3 >= 0 && y-3 >= 0 && board[x-3][y-3] == 0 ) {
							hardMove[0] = x-3;
							hardMove[1] = y-3; 
							r = true;
						}	
					} } } 
			
			// searches the board down to the right
			if( x+1 < BOARDSIZE && y+1 < BOARDSIZE  && board[x+1][y+1] == color ) {
				pieces++;
				if( pieces == endCon ) {
					//check if next is open
					if( x+2 < BOARDSIZE && y+2 < BOARDSIZE && board[x+2][y+2] == 0 ) {
						hardMove[0] = x+2;
						hardMove[1] = y+2; 
						r = true;
					}	
					int i = 0;
					found = false;
					while(i < endCon && !found) {
						
						if((x-i) >= 0 && (y-i) >= 0 && board[x-i][y-i] == 0) {
							hardMove[0] = x-i;
							hardMove[1] = y-i;
							r = true;
							found = true;
						}
						
						i++;
					}
				}
				if( x+2 < BOARDSIZE && y+2 < BOARDSIZE && board[x+2][y+2] == color ) {
					pieces++;
					if( pieces == endCon ) {
						//check if next is open
						if( x+3 < BOARDSIZE && y+3 < BOARDSIZE && board[x+3][y+3] == 0 ) {
							hardMove[0] = x+3;
							hardMove[1] = y+3; 
							r = true;
						}
						int i = 0;
						found = false;
						while(i < endCon && !found) {
							
							if((x-i) >= 0 && (y-i) >= 0 && board[x-i][y-i] == 0) {
								hardMove[0] = x-i;
								hardMove[1] = y-i;
								r = true;
								found = true;
							}
							
							i++;
						}
					} } } 
		} else {
			pieces = 0;
		}
		pieces = 0;
		
		if( board[x][y] == color ) {
			//increment number of pieces in a row 
			pieces++;
			// searches the board up to the right == check for 5 pieces in a row after each new piece found
			if( x-1 >= 0 && y+1 < BOARDSIZE && board[x-1][y+1] == color ) {
				pieces++;
				if( pieces == endCon ) {
					//check if next is open
					if( x-2 >= 0 && y+2 < BOARDSIZE && board[x-2][y+2] == 0 ) {
						hardMove[0] = x-2;
						hardMove[1] = y+2; 
						r = true;
					}	
				}
				if( x-2 >= 0 && y+2 < BOARDSIZE && board[x-2][y+2] == color ) {
					pieces++;
					if( pieces == endCon ) {
						//check if next is open
						if( x-3 >= 0 && y+3 < BOARDSIZE && board[x-3][y+3] == 0 ) {
							hardMove[0] = x-3;
							hardMove[1] = y+3; 
							r = true;
						}	
					} } } // searches the board down to the left
			if( x+1 < BOARDSIZE && y-1 >= 0 && board[x+1][y-1] == color ) {
				pieces++;
				if( pieces == endCon ) {
					//check if next is open
					if( x+2 >= 0 && y-2 < BOARDSIZE && board[x+2][y-2] == 0 ) {
						hardMove[0] = x+2;
						hardMove[1] = y-2; 
						r = true;
					}	
					int i = 0;
					found = false;
					while(i < endCon && !found) {
						
						if((x-i) >= 0 && (y+i) < BOARDSIZE && board[x-i][y+i] == 0) {
							hardMove[0] = x-i;
							hardMove[1] = y+i;
							r = true;
							found = true;
						}
						
						i++;
					}
				}
				if( x+2 < BOARDSIZE && y-2 >= 0 && board[x+2][y-2] == color ) {
					pieces++;
					if( pieces == endCon ) {
						//check if next is open
						if( x+3 >= 0 && y-3 < BOARDSIZE && board[x+3][y-3] == 0 ) {
							hardMove[0] = x+3;
							hardMove[1] = y-3; 
							r = true;
						}
						int i = 0;
						found = false;
						while(i < endCon && !found) {
							
							if((x-i) >= 0 && (y+i) < BOARDSIZE && board[x-i][y+i] == 0) {
								hardMove[0] = x-i;
								hardMove[1] = y+i;
								r = true;
								found = true;
							}
							
							i++;
						}
					} } } 
		} else{
			pieces = 0;
		}
		
		
		pieces = 0;
		return r;

	}

	/**
	 * Checks if the most recent piece played for the color 
	 * sent in has two pieces in a row. Only use as a 
	 * near-last-case scenerio if no better move can be found.
	 * 
	 * @param move the latest move by a player
	 * @param color that players color
	 * @return a boolean referring to if the condition was found.
	 */
	private boolean lookForTwo(int[] move, int color) {
		boolean found = false;
		
		int x = move[0], y = move[1];
		pieces = 0;
		boolean r = false;
		boolean done = false;
		if( board[x][y] == color ) {
			pieces++;
			// Search the board horizontally right:
			if( y+1 < BOARDSIZE && board[x][y+1] == color) {
				pieces++;
				if( pieces == 2 ) {
					//check if next is open
					if( y+2 < BOARDSIZE && board[x][y+2] == 0 ) {
						hardMove[0] = x;
						hardMove[1] = y+2;
						r = true;
					}	
				} 
			}else if (y+1 < BOARDSIZE && board[x][y+1] == 0) {
				hardMove[0] = x;
				hardMove[1] = y+1;
				done = true;
			}
			
			// Search the board horizontally left:
			if( y-1 >= 0 && board[x][y-1] == color ) {
				pieces++;
				if( pieces == 2 ) {
					//check if next is open
					if( y-2 >= BOARDSIZE && board[x][y-2] == 0 ) {
						hardMove[0] = x;
						hardMove[1] = y-2;
						return true;
					}else if(done) {
						return true;
					}
				} 
			}	
		}else {
			pieces = 0;
		}
		// Searched Horizontally 
		
		if( board[x][y] == color ) {
			pieces++;
			// Search the board vertically up:
			if( x-1 >= 0 && board[x-1][y] == color ) {
				pieces++;
				if( pieces == 2 ) {
					//check if next is open
					if( x-2 >= 0 && board[x-2][y] == 0 ) {
						hardMove[0] = x-2;
						hardMove[1] = y;
						return true;
					}	
				} 
			}else if (x-1 >= 0 && board[x-1][y] == 0) {
				hardMove[0] = x-1;
				hardMove[1] = y;
				done = true;
			}
			// Search the board vertically down:
			if( x+1 < BOARDSIZE && board[x+1][y] == color ) {
				pieces++;
				if( pieces == 2 ) {
					//check if next is open
					if( x+2 < BOARDSIZE && board[x+2][y] == 0 ) {
						hardMove[0] = x+2;
						hardMove[1] = y;
						return true;
					}else if(done) {
						return true;
					}
				} } 
		}else {
			pieces = 0;
		}
		//Searched Vertically 
		
		if( board[x][y] == color ) {
			//increment number of pieces in a row 
			pieces++;
			// searches the board up to the left == check for 5 pieces in a row after each new piece found
			if( x-1 >= 0 && y-1 >= 0 && board[x-1][y-1] == color   ) {
				pieces++;
				if( pieces == 2 ) {
					//check if next is open
					if( x-2 >= 0 && y-2 >= 0 && board[x-2][y-2] == 0 ) {
						hardMove[0] = x-2;
						hardMove[1] = y-2;
						return true;
					}	
				} 
			}else if ( x-1 >= 0 && y-1 >= 0 && board[x-1][y-1] == 0 ) {
				hardMove[0] = x-1;
				hardMove[1] = y-1;
				done = true;
			}
			
			// searches the board down to the right
			if( x+1 < BOARDSIZE && y+1 < BOARDSIZE  && board[x+1][y+1] == color ) {
				pieces++;
				if( pieces == 2 ) {
					//check if next is open
					if( x+2 < BOARDSIZE && y+2 < BOARDSIZE && board[x+2][y+2] == 0 ) {
						hardMove[0] = x+2;
						hardMove[1] = y+2; 
						return true;
					}else if(done) {
						return true;
					}
				} } 
		} else {
			pieces = 0;
		}	
		
		pieces = 0;
		if( board[x][y] == color ) {
			//increment number of pieces in a row 
			pieces++;
			// searches the board up to the right == check for 5 pieces in a row after each new piece found
			if( x-1 >= 0 && y+1 < BOARDSIZE && board[x-1][y+1] == color ) {
				pieces++;
				if( pieces == 2 ) {
					//check if next is open
					if( x-2 >= 0 && y+2 < BOARDSIZE && board[x-2][y+2] == 0 ) {
						hardMove[0] = x-2;
						hardMove[1] = y+2; 
						return true;
					}	
				} 
			}else if ( x-1 >= 0 && y+1 < BOARDSIZE && board[x-1][y+1] == 0 ) {
				hardMove[0] = x-1;
				hardMove[1] = y-1;
				done = true;
			}
			// searches the board down to the left
			if( x+1 < BOARDSIZE && y-1 >= 0 && board[x+1][y-1] == color ) {
				pieces++;
				if( pieces == 2 ) {
					//check if next is open
					if( x+2 < BOARDSIZE && y-2 >= 0 && board[x+2][y-2] == 0 ) {
						hardMove[0] = x+2;
						hardMove[1] = y-2; 
						return true;
					}else if(done) {
						return true;
					}
				}
			}
		} else{
			pieces = 0;
		}
		
		pieces = 0;
		return r;

	}

	/**
	 * Updates the representation of the board that this 
	 * AI accesses.
	 * 
	 * @param b the color of the piece
	 * @param r the row of the piece
	 * @param c the column of the piece
	 */
	public void updateBoard(int b, int r, int c) {
		board[r][c] = b;
		enemyLastMove[0] = r;
		enemyLastMove[1] = c;
	}





} 